#include "SqlManager.h"
#include <iostream>

// Initialize pointer to zero so that it can be inialized in first call to getInstance
SqlManager* SqlManager::instance = 0;

SqlManager::SqlManager() {

}

SqlManager* SqlManager::getInstance() {
	if (!instance) {
		instance = new SqlManager();
	}

	return instance;
}

bool SqlManager::OpenDatabase() {
	// Connect to database
	int exitCode = 0;
	exitCode = sqlite3_open(dbPath.c_str(), &database);

	if (exitCode) {
		std::cout << "Error opening sql databse: " << sqlite3_errmsg(database) << std::endl;
		return false;
	}

	return true;
}

void SqlManager::CloseDatabase() {
	sqlite3_close(database);
}

void SqlManager::RecordWin(bool player) {
	OpenDatabase();

	int exitCode = 0;
	char* errorMessage;
	std::string query;
	if (player) {
		query = "UPDATE Wins SET PlayerWins = PlayerWins + 1;";
	}
	else {
		query = "UPDATE Wins SET BotWins = BotWins + 1;";
	}

	exitCode = sqlite3_exec(database, query.c_str(), NULL, NULL, &errorMessage);
	if (exitCode != SQLITE_OK) {
		std::cout << "Error updating wins." << std::endl;
		sqlite3_free(errorMessage);
	}

	CloseDatabase();
}

void SqlManager::RecordCardsPlayed(int amount, int round) {
	OpenDatabase();

	int exitCode = 0;
	char* errorMessage;
	std::string rString = "Round_" + std::to_string(round);
	std::string query = "UPDATE CardsPlayed SET " + rString + " = " + rString + " + " + std::to_string(amount);

	exitCode = sqlite3_exec(database, query.c_str(), NULL, NULL, &errorMessage);
	if (exitCode != SQLITE_OK) {
		std::cout << "Error updating cards played." << std::endl;
		sqlite3_free(errorMessage);
	}

	CloseDatabase();
}

void SqlManager::RecordRarities(int commons, int uncommons, int rares) {
	OpenDatabase();

	int exitCode = 0;
	char* errorMessage;
	std::string query = "UPDATE Rarities SET Commons = Commons + " + std::to_string(commons) + ";"
		"UPDATE Rarities SET Uncommons = Uncommons + " + std::to_string(uncommons) + ";"
		"UPDATE Rarities SET Rares = Rares + " + std::to_string(rares) + ";";

	exitCode = sqlite3_exec(database, query.c_str(), NULL, NULL, &errorMessage);
	if (exitCode != SQLITE_OK) {
		std::cout << "Error updating rarities." << std::endl;
		sqlite3_free(errorMessage);
	}

	CloseDatabase();
}

void SqlManager::RecordDestroyed(int playerAmount, int botAmount) {
	OpenDatabase();

	int exitCode = 0;
	char* errorMessage;
	std::string query = "UPDATE Destroyed SET Player = Player + " + std::to_string(playerAmount) + ";"
		"UPDATE Destroyed SET Bot = Bot + " + std::to_string(botAmount) + ";";

	exitCode = sqlite3_exec(database, query.c_str(), NULL, NULL, &errorMessage);
	if (exitCode != SQLITE_OK) {
		std::cout << "Error updating rarities." << std::endl;
		sqlite3_free(errorMessage);
	}

	CloseDatabase();
}