#ifndef _CARDDATA_H_
#define _CARDDATA_H_

#include "Card.h"
#include <string>

// Purely data version of the Card class
class CardData {
	int id;
	std::string name;
	std::string description;
	std::string characterArtPath;
	CardClass cardClass;
	int value;
	int rarity;
	int allyAnthem;
	int enemyAnthem;
	bool destroy;
public:
	CardData(json::JSON cardJson);
	const std::string GetName();
	const std::string GetDescription();
	const std::string GetCharacterArtPath();
	const CardClass GetCardClass();
	const int GetValue();
	const int GetAllyAnthem();
	const int GetEnemyAnthem();
	const bool GetDestroy();
	const int GetRarity();
	const int GetID();
};

#endif _CARDDATA_H_
