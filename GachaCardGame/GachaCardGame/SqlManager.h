#pragma once
#ifndef _SQLMANAGER_H_
#define _SQLMANAGER_H_

#include "sqlite3.h"

#include <string>

class SqlManager
{
	const std::string dbPath = "Gacha.db";
	sqlite3* database;
	static SqlManager* instance;
	bool OpenDatabase();
	void CloseDatabase();
	SqlManager();

public:
	static SqlManager* getInstance();

	void RecordWin(bool player);
	void RecordCardsPlayed(int amount, int round); // number of cards played per-round
	void RecordRarities(int commons, int uncommons, int rares); // Number of each rarity played in a game
	void RecordDestroyed(int playerAmount, int botAmount); // Number of cards destroyed in a game
};

#endif // !_SQLMANAGER_H_

