#ifndef _CARD_H_
#define _CARD_H_

#include "json.hpp"
#include <string>
#include <SFML/Graphics.hpp>

//forward declare CardData
class CardData;

enum CardClass { Melee, Ranged, Siege };

class Card {
	// Card border
	sf::RectangleShape borderRect;

	// Card class symbol
	sf::RectangleShape classSymbolRect;
	sf::CircleShape classSymbolBackground;
	void SetClassSymbol();

	// Card Value
	sf::RectangleShape valueRect;
	sf::Text valueText;

	// Card description
	sf::Text descriptionText;
	std::string FormDescriptionText();

	// Card character
	sf::Texture characterArt;
	sf::RectangleShape characterRect;
	sf::RectangleShape characterBackground;

	int id;
	std::string name;
	int rarity;
	CardClass cardClass;
	std::string description;
	int value;
	sf::Vector2f position;
	int instanceID;
	int allyAnthem;
	int enemyAnthem;
	int boardAnthem; // the anthem on the current board
	bool destroy;

	void InitializeGraphic(std::string characterArtPath);

public:
	Card(CardClass cardClass, std::string description, std::string characterArtPath, int value);
	Card(CardData* card);
	//Card(json::JSON cardJson);
	~Card();
	void SetPosition(sf::Vector2f pos);
	void Display(sf::RenderWindow& window);
	int GetInstanceID();
	int GetValue();
	CardClass GetClass();
	sf::FloatRect GetSpriteBounds();
	static CardClass GetCardClass(int i);
	int GetAllyAnthem();
	int GetEnemyAnthem();
	void SetBoardAnthem(int anthem);
	bool GetDestroy();
	std::string GetName();
	int GetID();
	int GetRarity();
};

#endif // !_CARD_H