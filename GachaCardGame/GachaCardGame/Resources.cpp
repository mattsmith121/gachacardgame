#include "Resources.h"
#include "json.hpp"

#include <fstream>

// Initialize pointer to zero so that it can be inialized in first call to getInstance
Resources* Resources::instance = 0;

Resources* Resources::getInstance() {
	if (!instance) {
		instance = new Resources();
	}

	return instance;
}

Resources::Resources() {
	// Load Font
	if (!baseFont.loadFromFile("SansitaSwashed-Regular.ttf")) {
		// error
	}

#pragma region CardFiles
	// Load CardBorderArt
	if (!cardBorderArt.loadFromFile(cardBorderFile)) {
		// error
	}
	cardBorderArt.setSmooth(true);

	// Load Class Icons
	if (!meleeIcon.loadFromFile(meleeIconFile)) {
		// error
	}
	meleeIcon.setSmooth(true);

	if (!siegeIcon.loadFromFile(siegeIconFile)) {
		// error
	}
	siegeIcon.setSmooth(true);

	if (!rangeIcon.loadFromFile(rangeIconFile)) {
		// error
	}
	rangeIcon.setSmooth(true);

	// Load Value icon
	if (valueIcon.loadFromFile(valueIconFile)) {
		// error
	}
	valueIcon.setSmooth(true);
#pragma endregion CardFiles

	// Load in the card list
	LoadCardList();
}

void Resources::LoadCardList() {
	// Load cards from CardList.json
	std::ifstream inputStreamSettings("CardList.json");
	std::string cardListString((std::istreambuf_iterator<char>(inputStreamSettings)),
		std::istreambuf_iterator<char>());

	json::JSON jsonCardList = json::JSON::Load(cardListString);

	// Load in the list of cards
	auto cardsRange = jsonCardList;
	for (int i = 0; i < (int)cardsRange.size(); i++) {
		CardData* card = new CardData(cardsRange[i]);
		if (card->GetRarity() == 0) {
			cardList.commons.push_back(card);
		}
		else if (card->GetRarity() == 1) {
			cardList.uncommons.push_back(card);
		}
		else {
			cardList.rares.push_back(card);
		}
	}
}

sf::Font* Resources::GetBaseFont() {
	return &baseFont;
}

sf::Texture* Resources::GetCardBorderArt() {
	return &cardBorderArt;
}

sf::Texture* Resources::GetMeleeIcon() {
	return &meleeIcon;
}

sf::Texture* Resources::GetRangeIcon() {
	return &rangeIcon;
}

sf::Texture* Resources::GetSiegeIcon() {
	return &siegeIcon;
}

sf::Texture* Resources::GetValueIcon() {
	return &valueIcon;
}

sf::Texture* Resources::GetClassIcon(CardClass cardClass) {
	if (cardClass == CardClass::Melee) {
		return this->GetMeleeIcon();
	}
	else if (cardClass == CardClass::Ranged) {
		return this->GetRangeIcon();
	}
	else {
		return this->GetSiegeIcon();
	}
}

std::vector<CardData*>* Resources::GetCommons() {
	// Check if card list has been loaded already
	CheckInitialized();
	return &(cardList.commons);
}

std::vector<CardData*>* Resources::GetUncommons() {
	// Check if card list has been loaded already
	CheckInitialized();
	return &(cardList.uncommons);
}

std::vector<CardData*>* Resources::GetRares() {
	// Check if card list has been loaded already
	CheckInitialized();
	return &(cardList.rares);
}

CardData* Resources::GetCard(std::string cardName)
{
	CheckInitialized();

	// Check commons
	for (int i = 0; i < cardList.commons.size(); i++) {
		if (cardList.commons[i]->GetName() == cardName) {
			return cardList.commons[i];
		}
	}

	// Check uncommons
	for (int i = 0; i < cardList.uncommons.size(); i++) {
		if (cardList.uncommons[i]->GetName() == cardName) {
			return cardList.uncommons[i];
		}
	}

	// Check rares
	for (int i = 0; i < cardList.rares.size(); i++) {
		if (cardList.rares[i]->GetName() == cardName) {
			return cardList.rares[i];
		}
	}

	return NULL;
}

CardData* Resources::GetCard(int id) {
	CheckInitialized();

	// Check commons
	for (int i = 0; i < cardList.commons.size(); i++) {
		if (cardList.commons[i]->GetID() == id) {
			return cardList.commons[i];
		}
	}

	// Check uncommons
	for (int i = 0; i < cardList.uncommons.size(); i++) {
		if (cardList.uncommons[i]->GetID() == id) {
			return cardList.uncommons[i];
		}
	}

	// Check rares
	for (int i = 0; i < cardList.rares.size(); i++) {
		if (cardList.rares[i]->GetID() == id) {
			return cardList.rares[i];
		}
	}

	return NULL;
}

// Safety check, just in case
void Resources::CheckInitialized() {
	// If cards have been initialized
	// commons will NEVER be 0
	if (cardList.commons.size() <= 0) {
		LoadCardList();
	}
}
