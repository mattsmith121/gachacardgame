#ifndef _PARTICIPANT_H_
#define _PARTICIPANT_H_

#include <vector>
#include "Card.h"
#include "Board.h"

class Participant {

protected:
	int cardsPlayed;
	int commonsPlayed, uncommonsPlayed, raresPlayed;
	int destroyedPlayed;
	virtual ~Participant() = 0;
	std::vector<Card*> hand;
	std::vector<Card*> deck;
	bool player;
	bool Load(json::JSON &jObject);

public:
	void DrawCard();
	void RemoveCardFromHand(Card* card);
	void PlayCard(Card* card, Board* board);
	void CreateDeck(); // Create a random deck
	void LoadDeck(std::string deckPath); // Load deck from file
	void ClearDeckAndHand();
	virtual void Save(json::JSON &object);
	int GetCommonsPlayed();
	int GetUncommonsPlayed();
	int GetRaresPlayed();
	int GetCardsPlayed();
	int GetDestroyedPlayed();
	void ResetData(bool newgame);
};

#endif
