#ifndef _BOARD_H_
#define _BOARD_H_

#include <vector>
#include "Card.h"
#include <SFML/Graphics.hpp>

// Forward declare GameEngine
class GameEngine;

class Board{
	GameEngine* gameEngine;
	std::vector<Card*> playerBoard;
	std::vector<Card*> botBoard;
	sf::RectangleShape passRect;
	sf::Text passText;
	sf::RectangleShape saveRect;
	sf::Text saveText;
	sf::RectangleShape fieldRect;
	sf::Text fieldName;
	sf::Text playerScoreText;
	sf::Text playerScoreTitle;
	sf::Text botScoreText;
	sf::Text botScoreTitle;
	int botAnthem;
	int playerAnthem;

	// Destroy Text
	sf::Text destroyText;

	std::string GetFieldName(int index);
	void SetupScores();
	void DrawField(sf::RenderWindow &window);
	void DrawCards(sf::RenderWindow &window, bool player);
	void DrawPassTurn(sf::RenderWindow &window);
	void DrawSave(sf::RenderWindow &window);
	void RandomDestroy(bool playerBoard);
	void GetCardTotals(std::vector<Card*>* board, int* melee, int* ranged, int* siege);

public:
	Board(GameEngine* gameEngine);
	~Board();
	void AddCard(bool player, Card* card);
	void RemoveCard(bool player, Card* card);
	void Display(sf::RenderWindow& window);
	int GetPlayerScore();
	int GetBotScore();
	bool ClickedPass(sf::Vector2f coords);
	bool ClickedSave(sf::Vector2f coords);
	void ClearBoard();
	void UpdateAnthems();
	Card* ClickedOppBoard(sf::Vector2f coords);
	void Save(json::JSON &saveFile);
	void Load(json::JSON &jPlayerBoard, json::JSON &jBotBoard);
};


#endif
