#include "CardData.h"
#include "json.hpp"

CardData::CardData(json::JSON card) {
    if (card.hasKey("ID")) {
        this->id = card["ID"].ToInt();
    }
    else {
        this->id = -1; // -1 for invalid card
    }
    
    if (card.hasKey("Name")) {
        this->name = card["Name"].ToString();
    }
    else {
        this->name = "Invalid Card";
    }

    if (card.hasKey("CardClass")) {
        this->cardClass = Card::GetCardClass((card["CardClass"].ToInt()));
    }
    else {
        this->cardClass = CardClass::Melee;
    }
    
    if (card.hasKey("Description")) {
        this->description = card["Description"].ToString();
    }
    else {
        this->description = "Card is invalid.";
    }

    if (card.hasKey("Value")) {
        this->value = card["Value"].ToInt();
    }
    else {
        this->value = 0;
    }

    if (card.hasKey("Rarity")) {
        this->rarity = card["Rarity"].ToInt();
    }
    else {
        this->rarity = 0;
    }
    
    if (card.hasKey("Sprite")) {
        this->characterArtPath = card["Sprite"].ToString();
    }
    else {
        this->characterArtPath = "card_nightmare_breakfast.png";
    }
    
    if (card.hasKey("AllyAnthem")) {
        this->allyAnthem = card["AllyAnthem"].ToInt();
    }
    else {
        this->allyAnthem = 0;
    }

    if (card.hasKey("EnemyAnthem")) {
        this->enemyAnthem = card["EnemyAnthem"].ToInt();
    }
    else {
        this->enemyAnthem = 0;
    }

    if (card.hasKey("Destroy")) {
        this->destroy = card["Destroy"].ToBool();
    }
    else {
        this->destroy = false;
    }
}

const std::string CardData::GetName()
{
    return name;
}

const std::string CardData::GetDescription()
{
    return description;
}

const std::string CardData::GetCharacterArtPath()
{
    return characterArtPath;
}

const CardClass CardData::GetCardClass()
{
    return cardClass;
}

const int CardData::GetValue()
{
    return value;
}

const int CardData::GetAllyAnthem()
{
    return allyAnthem;
}

const int CardData::GetEnemyAnthem()
{
    return enemyAnthem;
}

const bool CardData::GetDestroy()
{
    return destroy;
}

const int CardData::GetID() {
    return id;
}

const int CardData::GetRarity() {
    return rarity;
}
