// SFML_Template.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
#include <SFML/Window.hpp>
#include <SFML/Graphics.hpp>
#include "Card.h"
#include "Board.h"
#include "GameEngine.h"

// To make sure your project compiles and runs, you will need to extract the SFML folder at the solution folder.

int main()
{
    //Card card1 = Card(CardClass::Melee, "This is the description.", "Goddess.png", 10);
    //Card card2 = Card(CardClass::Melee, "This is the description.", "Goddess.png", 5);
    //Card card3 = Card(CardClass::Ranged, "This is the description.", "Goddess.png", 7);
    //Card card4 = Card(CardClass::Siege, "This is the description.", "Goddess.png", 1);

    //Board board = Board();
    //board.AddCard(true, &card1);
    //board.AddCard(true, &card2);
    //board.AddCard(true, &card3);
    //board.AddCard(true, &card4);
    //board.AddCard(false, &card1);
    //board.AddCard(false, &card2);
    //board.AddCard(false, &card3);
    //board.AddCard(false, &card4);
    //board.AddCard(false, &card4);

    //// run the program as long as the window is open
    //while (window.isOpen())
    //{
    //    // check all the window's events that were triggered since the last iteration of the loop
    //    sf::Event event;
    //    while (window.pollEvent(event))
    //    {
    //        // "close requested" event: we close the window
    //        if (event.type == sf::Event::Closed)
    //            window.close();
    //    }

    //    // clear the window with black color
    //    window.clear(sf::Color::Black);

    //    // draw everything here...
    //    //window.draw(sprite);
    //    //window.draw(rect);
    //    //card1.Display(window);
    //    board.Display(window);

    //    // end the current frame
    //    window.display();
    //}

    GameEngine* gameEngine = new GameEngine();
    gameEngine->GameLoop();
    delete(gameEngine);
}
