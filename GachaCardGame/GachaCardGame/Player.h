#ifndef _PLAYER_H_
#define _PLAYER_H_

#include "Participant.h"
#include "Card.h"

class Player : public Participant {
	
	const float handPosTop = 150.0f;

public:
	Player();
	~Player();
	Card* ClickedHand(sf::Vector2f coords);
	void Display(sf::RenderWindow &window);
	void Save(json::JSON &document) override;
	bool Load(json::JSON &jPlayer);
};

#endif