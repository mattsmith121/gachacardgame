#ifndef _GAMEENGINE_H_
#define _GAMENEGINE_H_

#include "Card.h"
#include "Player.h"
#include "Bot.h"
#include "Board.h"
#include <SFML/Graphics.hpp>

enum GameState { StartScreen, PlayerTurn, BotTurn, EndGame };

class GameEngine {
	GameState gameState;
	Player* player;
	Bot* bot;
	Board* board;
	bool playerPass;
	int playerWins, botWins;
	int activePlayerDeck, activeBotDeck;
	bool destroyState;
	const std::string SaveFile = "SaveFile.json";
	void TransitionToGame(); // start screen to game screen
	void HandleMouseClick(sf::Vector2f coords, sf::RenderWindow &window);
	void EndRound();

	// EndGame Screen
	sf::RectangleShape playAgainRect;
	sf::Text playAgainText;
	sf::RectangleShape egQuitRect;
	sf::Text egQuitText;
	sf::Text winnerText;
	void SetupEndGameDisplay();
	void DisplayEndGameScreen(sf::RenderWindow &window);

	// Start Screen
	sf::RectangleShape playRect;
	sf::RectangleShape quitRect;
	sf::RectangleShape loadRect;
	sf::Text playText;
	sf::Text quitText;
	sf::Text loadText;
	sf::Text titleText;
	sf::Text playerText;
	sf::Text botText;
	sf::Text goodDeck;
	sf::Text badDeck;
	sf::Text opDeck;
	sf::Text randomDeck;
	sf::Text gameTitle;
	sf::RectangleShape botBadRect;
	sf::RectangleShape botGoodRect;
	sf::RectangleShape botOPRect;
	sf::RectangleShape botRandomRect;
	sf::RectangleShape playerBadRect;
	sf::RectangleShape playerGoodRect;
	sf::RectangleShape playerOPRect;
	sf::RectangleShape playerRandomRect;
	void SetupStartDisplay();
	void DisplayStartScreen(sf::RenderWindow &window);	
	void SwitchDeck(bool player, int deck);
	void UpdateDeckColours();
	std::string GetDeckString(int deck);
	void SaveGame();
	void LoadGame();

	void HandleStartClick(sf::Vector2f &coords, sf::RenderWindow &window);
	void HandleGameClick(sf::Vector2f &coords, sf::RenderWindow &window);
	void HandleEndClick(sf::Vector2f &coords, sf::RenderWindow &window);

public:
	GameEngine();
	~GameEngine();
	void GameLoop();
	int GetPlayerWins();
	int GetBotWins();
	void SetDestroyState(bool state);
	bool GetDestroyState();
};

#endif // !_GAMEENGINE_H_

