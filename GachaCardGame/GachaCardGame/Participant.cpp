#include "Participant.h"
#include "Resources.h"

#include <fstream>
#include <iostream>
#include <stdlib.h>
#include <chrono>
#include <vector>
#include <random>

Participant::~Participant() {
	ClearDeckAndHand();
}

void Participant::DrawCard() {
	// Note: the back of the deck is the top of the deck
	if (deck.size() <= 0) {
		return;
	}
	Card* topDeck = deck.back();
	deck.pop_back();
	hand.push_back(topDeck);
}

void Participant::CreateDeck() {
	for (int i = 0; i < 15; i++) {
		// Randomly select cards and add them to deck

		// figure out rarity
		srand(std::chrono::system_clock::now().time_since_epoch().count());
		int randRarity = rand() % 100 + 1; // Random id between 1 and 100
		std::vector<CardData*>* cardList;
		if (randRarity <= 70) {
			// Common
			cardList = Resources::getInstance()->GetCommons();
		}
		else if (randRarity <= 99) {
			// Uncommon
			cardList = Resources::getInstance()->GetUncommons();
		}
		else {
			// rarity = 100 => rare
			cardList = Resources::getInstance()->GetRares();
		}

		// get random card of that rarity
		int randCard = rand() % cardList->size();
		deck.push_back(new Card((*cardList)[randCard]));
	}
}

void Participant::LoadDeck(std::string deckPath) {
	// Load the deck from the given path
	std::ifstream inputStreamSettings(deckPath);
	std::string cardListString((std::istreambuf_iterator<char>(inputStreamSettings)),
		std::istreambuf_iterator<char>());

	json::JSON jsonCardList = json::JSON::Load(cardListString);

	// Deck format
	if (!jsonCardList.hasKey("Cards")) {
		// error
		std::cout << "Deck in wrong format: " << deckPath << std::endl;
		CreateDeck();
		return;
	}

	// add all the cards to the deck
	auto cardsRange = jsonCardList["Cards"];
	for (int i = 0; i < (int)cardsRange.size(); i++) {
		int cardID = cardsRange[i].ToInt();
		deck.push_back(new Card(Resources::getInstance()->GetCard(cardID)));
	}

	// shuffle the deck
	unsigned seed = std::chrono::system_clock::now().time_since_epoch().count();
	std::shuffle(deck.begin(), deck.end(), std::default_random_engine(seed));
}

void Participant::PlayCard(Card* card, Board* board) {
	// Update data
	if (card->GetRarity() == 0) {
		commonsPlayed++;
	}
	else if (card->GetRarity() == 1) {
		uncommonsPlayed++;
	}
	else {
		raresPlayed++;
	}

	if (card->GetDestroy()) {
		destroyedPlayed++;
	}

	cardsPlayed++;

	// Remove card from player hand
	RemoveCardFromHand(card);

	// Add card to board
	board->AddCard(player, card);
	
	// Draw card
	DrawCard();
}

void Participant::RemoveCardFromHand(Card* card) {
	// Remove card from hand
	int index = -1;
	for (int i = 0; i < hand.size(); i++) {
		if (hand[i]->GetInstanceID() == card->GetInstanceID()) {
			index = i;
			break;
		}
	}

	if (index != -1) {
		hand.erase(hand.begin() + index);
	}
}

void Participant::ClearDeckAndHand() {
	// Clear hand
	for (int i = 0; i < hand.size(); i++) {
		delete(hand[i]);
	}
	hand.clear();

	// Clear deck
	for (int i = 0; i < deck.size(); i++) {
		delete(deck[i]);
	}
	deck.clear();
}

void Participant::Save(json::JSON &object)
{
	// Save hand
	json::JSON jHand = json::JSON::Array();
	for (int i = 0; i < hand.size(); i++) {
		jHand[i] = hand[i]->GetID();
	}
	object["Hand"] = jHand;

	// Save deck
	json::JSON jDeck = json::JSON::Array();
	for (int i = 0; i < deck.size(); i++) {
		jDeck[i] = deck[i]->GetID();
	}
	object["Deck"] = jDeck;

	// Save data
	object["Commons"] = commonsPlayed;
	object["Uncommons"] = uncommonsPlayed;
	object["Rares"] = raresPlayed;
	object["CardsPlayed"] = cardsPlayed;
	object["Destroyed"] = destroyedPlayed;
}

bool Participant::Load(json::JSON &jObject)
{
	if (!jObject.hasKey("Deck") ||
		!jObject.hasKey("Hand") ||
		!jObject.hasKey("Commons") ||
		!jObject.hasKey("Uncommons") ||
		!jObject.hasKey("Rares") ||
		!jObject.hasKey("Destroyed") ||
		!jObject.hasKey("CardsPlayed")) {
		std::cout << "Error in save file." << std::endl;
		return false;
	}

	// Load Deck
	// this will load in same order as it was saved in
	json::JSON jDeck = jObject["Deck"];
	for (int i = 0; i < jDeck.size(); i++) {
		deck.push_back(new Card(Resources::getInstance()->GetCard(jDeck[i].ToInt())));
	}

	// Load Hand
	// order doesn't matter, but same order anyway
	json::JSON jHand = jObject["Hand"];
	for (int i = 0; i < jHand.size(); i++) {
		hand.push_back(new Card(Resources::getInstance()->GetCard(jHand[i].ToInt())));
	}

	// Load Data
	commonsPlayed = jObject["Commons"].ToInt();
	uncommonsPlayed = jObject["Uncommons"].ToInt();
	raresPlayed = jObject["Rares"].ToInt();
	cardsPlayed = jObject["CardsPlayed"].ToInt();
	destroyedPlayed = jObject["Destroyed"].ToInt();

	return true;
}

int Participant::GetCommonsPlayed()
{
	return commonsPlayed;
}

int Participant::GetUncommonsPlayed()
{
	return uncommonsPlayed;
}

int Participant::GetRaresPlayed()
{
	return raresPlayed;
}

int Participant::GetCardsPlayed() {
	return cardsPlayed;
}

int Participant::GetDestroyedPlayed() {
	return destroyedPlayed;
}

void Participant::ResetData(bool newgame) {
	// always reset cards played
	cardsPlayed = 0;

	// if a new game, also reset rarities
	if (newgame) {
		commonsPlayed = 0;
		raresPlayed = 0;
		uncommonsPlayed = 0;
		destroyedPlayed = 0;
	}
}
