#include "Card.h"
#include "Resources.h"
#include "CardData.h"

#include <stdlib.h>
#include <chrono>

Card::Card(CardClass cardClass, std::string description, std::string characterArtPath, int value) {
	this->cardClass = cardClass;
	this->description = description;
    this->value = value;
    boardAnthem = 0;

    // Get identifier
    srand(std::chrono::system_clock::now().time_since_epoch().count());
    instanceID = rand(); // Random id between 0 and RAND_MAX

    InitializeGraphic(characterArtPath);
}

Card::Card(CardData* card)
{
    this->name = card->GetName();
    this->cardClass = card->GetCardClass();
    this->description = card->GetDescription();
    this->value = card->GetValue();
    this->allyAnthem = card->GetAllyAnthem();
    this->enemyAnthem = card->GetEnemyAnthem();
    this->destroy = card->GetDestroy();
    this->id = card->GetID();
    this->rarity = card->GetRarity();
    boardAnthem = 0;

    // Get identifier
    srand(std::chrono::system_clock::now().time_since_epoch().count());
    instanceID = rand(); // Random id between 0 and RAND_MAX

    InitializeGraphic(card->GetCharacterArtPath());
}

// Keeping this in just in case, but unused for now
//Card::Card(json::JSON cardJson) {
//    this->name = cardJson["Name"].ToString();
//	this->cardClass = Card::GetCardClass((cardJson["CardClass"].ToInt()));
//	this->description = cardJson["Description"].ToString();
//	this->value = cardJson["Value"].ToInt();
//    boardAnthem = 0;
//    if (cardJson.hasKey("AllyAnthem")) {
//        this->allyAnthem = cardJson["AllyAnthem"].ToInt();
//    }
//    else {
//        this->allyAnthem = 0;
//    }
//    if (cardJson.hasKey("EnemyAnthem")) {
//        this->enemyAnthem = cardJson["EnemyAnthem"].ToInt();
//    }
//    else {
//        this->enemyAnthem = 0;
//    }
//    if (cardJson.hasKey("Destroy")) {
//        this->destroy = cardJson["Destroy"].ToBool();
//    }
//    else {
//        this->destroy = false;
//    }
//	
//    InitializeGraphic(cardJson["Sprite"].ToString());
//}

void Card::InitializeGraphic(std::string characterArtPath) {
    // Load character art
    if (!this->characterArt.loadFromFile(characterArtPath))
    {
        // error...
    }
    characterArt.setSmooth(true);
    characterRect.setSize(sf::Vector2f(140.0f, 110.0f));
    characterRect.setTexture(&characterArt);

    // Character background
    characterBackground.setSize(sf::Vector2f(150.0f, 150.0f));
    characterBackground.setFillColor(sf::Color(0, 0, 0));

    // Card border
    borderRect.setSize(sf::Vector2f(180.0f, 230.0f));
    borderRect.setTexture(Resources::getInstance()->GetCardBorderArt());

    // Card value 
    valueRect.setSize(sf::Vector2f(30.0f, 30.0f));
    valueRect.setTexture(Resources::getInstance()->GetValueIcon());

    // Set value text
    valueText.setFont((*Resources::getInstance()->GetBaseFont()));
    valueText.setString(std::to_string(GetValue()));
    valueText.setCharacterSize(16);

    // Set description
    descriptionText.setFont((*Resources::getInstance()->GetBaseFont()));
    descriptionText.setString(FormDescriptionText());
    descriptionText.setCharacterSize(12);

    // Set class icon
    SetClassSymbol();

    SetPosition(sf::Vector2f(50.0f, 200.0f));
}

Card::~Card() {

}

void Card::SetPosition(sf::Vector2f pos) {
    position = pos;

    // Change position of all rectangles
    // - character rect
    characterRect.setPosition(sf::Vector2f(position.x + 20.0f, position.y + 30.0f));
    characterBackground.setPosition(sf::Vector2f(position.x + 20.0f, position.y + 10.0f));
    // - border rect
    borderRect.setPosition(position);
    // - value rect and text
    valueRect.setPosition(position.x + 5.0f, position.y + 5.0f);
    valueText.setPosition(position.x + 15.0f, position.y + 10.0f);
    // - class rect and background
    classSymbolRect.setPosition(sf::Vector2f(position.x + 150.0f, position.y + 10.0f));
    classSymbolBackground.setPosition(sf::Vector2f(position.x + 145.0f, position.y + 5.0f));
    // - description
    descriptionText.setPosition(position.x + 20.0f, position.y + 150.0f);

}

void Card::Display(sf::RenderWindow& window) {
    // draw character background
    window.draw(characterBackground);
    // draw card character
    window.draw(characterRect);
    // draw card border
    window.draw(borderRect);
    // draw card symbol
    window.draw(classSymbolBackground);
    window.draw(classSymbolRect);
    // draw card value
    window.draw(valueRect);
    valueText.setString(std::to_string(GetValue())); // have to update value for anthems
    window.draw(valueText);
    // draw card description
    window.draw(descriptionText);
}

void Card::SetClassSymbol() {
   
    classSymbolRect.setSize(sf::Vector2f(20.0f, 20.0f));
    classSymbolRect.setTexture(Resources::getInstance()->GetClassIcon(cardClass));

    // Create background
    classSymbolBackground.setFillColor(sf::Color(0, 0, 0));
    classSymbolBackground.setOutlineThickness(3.0f);
    classSymbolBackground.setOutlineColor(sf::Color(82, 47, 2));
    classSymbolBackground.setRadius(15.0f);
}

std::string Card::FormDescriptionText() {
    std::string desc = name + "\n" + description;
    // Add Ally Anthem text
    if (allyAnthem != 0) {
        desc += "\nAllies +" + std::to_string(allyAnthem);
    }

    // Add Enemy Anthem text
    if (enemyAnthem != 0) {
        desc += "\nEnemies " + std::to_string(enemyAnthem);
    }

    // Add Destroy text
    if (destroy) {
        desc += "\nDestroy Opponent's card";
    }

    return desc;
}

int Card::GetInstanceID() {
    return instanceID;
}

int Card::GetValue() {
    return value + boardAnthem;
}

CardClass Card::GetClass() {
    return cardClass;
}

sf::FloatRect Card::GetSpriteBounds() {
    // borderRect is the biggest sprite and will be used to determine clicks
    return borderRect.getGlobalBounds();
}

CardClass Card::GetCardClass(int i)
{
    // i can only be:
    // 0 - Melee
    // 1 - Ranged
    // 2 - Siege
    if (i < 0 || i > 2) {
        return CardClass::Melee; // melee will be default
    }
    return (CardClass)i;
}

int Card::GetAllyAnthem()
{
    return allyAnthem;
}

int Card::GetEnemyAnthem()
{
    return enemyAnthem;
}

void Card::SetBoardAnthem(int anthem) {
    boardAnthem = anthem;
}

bool Card::GetDestroy()
{
    return destroy;
}

std::string Card::GetName() {
    return name;
}

int Card::GetID() {
    return id;
}

int Card::GetRarity() {
    return rarity;
}
