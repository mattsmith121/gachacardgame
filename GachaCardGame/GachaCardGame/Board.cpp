#include "Board.h"
#include "Card.h"
#include "Resources.h"
#include "GameEngine.h"

#include <stdlib.h>
#include <chrono>

Board::Board(GameEngine* gameEngine) {
	this->gameEngine = gameEngine;
	// Set up board shape
	fieldRect.setFillColor(sf::Color());
	fieldRect.setSize(sf::Vector2f(200.0f, 900.0f));
	fieldRect.setOutlineColor(sf::Color(255, 0, 0));
	fieldRect.setOutlineThickness(5);

	// Set up field text
	fieldName.setFont((*Resources::getInstance()->GetBaseFont()));
	fieldName.setCharacterSize(16);

	// Set up pass button
	passRect.setFillColor(sf::Color(0, 0, 255));
	passRect.setSize(sf::Vector2f(180.0f, 60.0f));
	passRect.setPosition(sf::Vector2f(50.0f, 30.0f));
	passText.setFont((*Resources::getInstance()->GetBaseFont()));
	passText.setCharacterSize(20);
	passText.setString("Pass Turn");
	passText.setPosition(sf::Vector2f(90.0f, 45.0f));

	// Set up save button
	saveRect.setFillColor(sf::Color(0, 0, 255));
	saveRect.setSize(sf::Vector2f(180.0f, 60.0f));
	saveRect.setPosition(sf::Vector2f(1500.0f, 1000.0f));
	saveText.setFont((*Resources::getInstance()->GetBaseFont()));
	saveText.setCharacterSize(20);
	saveText.setString("Save Game");
	saveText.setPosition(sf::Vector2f(1540.0f, 1015.0f));

	// Set up score texts
	playerScoreTitle.setFont((*Resources::getInstance()->GetBaseFont()));
	playerScoreTitle.setString("Player");
	playerScoreTitle.setCharacterSize(20);
	playerScoreTitle.setPosition(1010.0f, 300.0f);
	playerScoreText.setFont((*Resources::getInstance()->GetBaseFont()));
	playerScoreText.setCharacterSize(16);
	playerScoreText.setPosition(1040.0f, 340.0f);

	botScoreTitle.setFont((*Resources::getInstance()->GetBaseFont()));
	botScoreTitle.setString("Opponent");
	botScoreTitle.setCharacterSize(20);
	botScoreTitle.setPosition(1000.0f, 600.0f);	
	botScoreText.setFont((*Resources::getInstance()->GetBaseFont()));
	botScoreText.setCharacterSize(20);
	botScoreText.setPosition(1040.0f, 640.0f);

	// Set up destroy text
	destroyText.setFont((*Resources::getInstance()->GetBaseFont()));
	destroyText.setString("Click an opponent's card to destroy it, or pass the turn.");
	destroyText.setCharacterSize(40);
	destroyText.setPosition(400.0f, 1000.0f);
}

Board::~Board() {

}

void Board::AddCard(bool player, Card* card) {
	if (player) {
		playerBoard.push_back(card);
		// Update anthems
		playerAnthem += card->GetAllyAnthem();
		botAnthem += card->GetEnemyAnthem();

		if (card->GetDestroy() && botBoard.size() > 0) {
			gameEngine->SetDestroyState(true);
		}
	}
	else {
		botBoard.push_back(card);
		// Update anthems
		botAnthem += card->GetAllyAnthem();
		playerAnthem += card->GetEnemyAnthem();

		// Check for destroy effect
		if (card->GetDestroy()) {
			// Bot just randomly destroys a card
			RandomDestroy(true);
		}
	}

	// Update anthems on cards in play
	UpdateAnthems();
}

void Board::RemoveCard(bool player, Card* card) {
	int index = -1;
	std::vector<Card*>* board;
	if (player) {
		board = &playerBoard;
	}
	else {
		board = &botBoard;
	}

	for (int i = 0; i < board->size(); i++) {
		// Need to know where in the area to clear
		// can tell exact card by id
		if ((*board)[i]->GetInstanceID() == card->GetInstanceID()) {
			index = i;
			break;
		}
	}

	if (player) {
		playerAnthem -= card->GetAllyAnthem();
		botAnthem -= card->GetEnemyAnthem();
	}
	else {
		botAnthem -= card->GetAllyAnthem();
		playerAnthem -= card->GetEnemyAnthem();
	}

	// Delete the card
	delete(card);

	if (index != -1) {
		board->erase(board->begin()+index);
	}

	// Update after card deleted and removed
	UpdateAnthems();
}

void Board::Display(sf::RenderWindow& window) {
	// Draw board
	DrawField(window);

	// Draw buttons for player
	DrawPassTurn(window);
	DrawSave(window);

	// Draw Cards
	DrawCards(window, true);
	DrawCards(window, false);

	// Display Score
	SetupScores();
	window.draw(playerScoreTitle);
	window.draw(playerScoreText);
	window.draw(botScoreTitle);
	window.draw(botScoreText);

	// Draw destroy text
	if (gameEngine->GetDestroyState()) {
		window.draw(destroyText);
	}
}

void Board::DrawField(sf::RenderWindow& window) {
	float fSpace = 220.0f;
	for (int i = 0; i < 6; i++) {
		// Draw the field
		sf::Vector2f position;
		if (i < 3) {
			position = sf::Vector2f(300.0f + fSpace * i, 60.0f);
		}
		else {
			// Seperate bot board from player board
			position = sf::Vector2f(500.0f + fSpace * i, 60.0f);
		}
		fieldRect.setPosition(position);
		window.draw(fieldRect);

		// Display name of field being drawn
		fieldName.setString(GetFieldName(i));
		fieldName.setPosition(sf::Vector2f(position.x + 75.0f, position.y));
		window.draw(fieldName);
	}
}

void Board::DrawCards(sf::RenderWindow& window, bool player) {
	int meleeTotal = 1, rangedTotal = 1, siegeTotal = 1;
	int melee = 1, ranged = 1, siege = 1;
	std::vector<Card*>* board = player ? &playerBoard : &botBoard;
	// This will only ever be O(15) => O(1) so is fine here
	GetCardTotals(board, &meleeTotal, &rangedTotal, &siegeTotal);
	for (int i = 0; i < board->size(); i++) {
		Card* temp = (*board)[i];
		if (temp->GetClass() == CardClass::Siege) {
			// Set position of card
			float xPos = player ? 300.0f : 1620.0f;
			// get divided position, subtract half card height, add field y position (plus a bit)
			float yPos = (900.0f / siegeTotal) * siege - 115.0f + 100.0f;
			temp->SetPosition(sf::Vector2f(xPos, yPos));
			siege++;
		}
		else if (temp->GetClass() == CardClass::Ranged) {
			// Set position of card
			float xPos = player ? 520.0f : 1400.0f;
			float yPos = (900.0f / rangedTotal) * ranged - 115.0f + 100.0f;
			temp->SetPosition(sf::Vector2f(xPos, yPos));
			ranged++;
		}
		else {
			// Set position of card
			float xPos = player ? 740.0f : 1180.0f;
			float yPos = (900.0f / meleeTotal) * melee - 115.0f + 100.0f;
			temp->SetPosition(sf::Vector2f(xPos, yPos));
			melee++;
		}
		// Draw card
		temp->Display(window);
	}
}

void Board::DrawPassTurn(sf::RenderWindow& window) {
	window.draw(passRect);
	window.draw(passText);
}

void Board::DrawSave(sf::RenderWindow& window) {
	window.draw(saveRect);
	window.draw(saveText);
}

void Board::RandomDestroy(bool player)
{
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	if (player && playerBoard.size() > 0) {
		int randCard = rand() % playerBoard.size(); // Random card on playerboard
		RemoveCard(true, playerBoard[randCard]);
	}
	else if (!player && botBoard.size() > 0) {
		int randCard = rand() % botBoard.size(); // Random card on playerboard
		RemoveCard(false, botBoard[randCard]);
	}
}

// Gets the name for the currently being drawn field
std::string Board::GetFieldName(int index) {
	switch (index)
	{
	case 0:
	case 5:
		return "Siege";
	case 1:
	case 4:
		return "Ranged";
	default:
		return "Melee";
	}
}

int Board::GetPlayerScore() {
	int total = 0;
	for (int i = 0; i < playerBoard.size(); i++) {
		total += playerBoard[i]->GetValue();
	}

	return total;
}

int Board::GetBotScore() {
	int total = 0;
	for (int i = 0; i < botBoard.size(); i++) {
		total += botBoard[i]->GetValue();
	}

	return total;
}

void Board::SetupScores() {
	// Player Score
	playerScoreText.setString(std::to_string(gameEngine->GetPlayerWins()));

	// Bot Score
	botScoreText.setString(std::to_string(gameEngine->GetBotWins()));
}

bool Board::ClickedPass(sf::Vector2f coords) {
	// Get bounds of pass button
	sf::FloatRect bounds = passRect.getGlobalBounds();
	// Check if coords inside bounds
	if (bounds.contains(coords)) {
		return true;
	}

	return false;
}

bool Board::ClickedSave(sf::Vector2f coords) {
	// Get bounds of pass button
	sf::FloatRect bounds = saveRect.getGlobalBounds();
	// Check if coords inside bounds
	if (bounds.contains(coords)) {
		return true;
	}

	return false;
}

void Board::ClearBoard() {
	// Any card removed from the board must be freed from memory
	// Clear player board
	for (int i = 0; i < playerBoard.size(); i++) {
		delete(playerBoard[i]);
	}
	playerBoard.clear();
	playerAnthem = 0;

	// Clear bot board
	for (int i = 0; i < botBoard.size(); i++) {
		delete(botBoard[i]);
	}
	botBoard.clear();
	botAnthem = 0;
}

void Board::UpdateAnthems()
{
	// Update player board
	for (int i = 0; i < playerBoard.size(); i++) {
		playerBoard[i]->SetBoardAnthem(playerAnthem);
	}

	// Update bot board
	for (int i = 0; i < botBoard.size(); i++) {
		botBoard[i]->SetBoardAnthem(botAnthem);
	}
}

Card* Board::ClickedOppBoard(sf::Vector2f coords)
{
	for (int i = 0; i < botBoard.size(); i++) {
		// get bounds of card
		sf::FloatRect bounds = botBoard[i]->GetSpriteBounds();

		if (bounds.contains(coords)) {
			// clicked this card
			return botBoard[i];
		}
	}

	return NULL;
}

void Board::Save(json::JSON &document) {
	// Save Player Board
	json::JSON jPlayerBoard = json::JSON::Array();
	for (int i = 0; i < playerBoard.size(); i++) {
		jPlayerBoard[i] = playerBoard[i]->GetName();
	}
	document["PlayerBoard"] = jPlayerBoard;

	// Save Bot Board
	json::JSON jBotBoard = json::JSON::Array();
	for (int i = 0; i < botBoard.size(); i++) {
		jBotBoard[i] = botBoard[i]->GetName();
	}
	document["BotBoard"] = jBotBoard;
}

void Board::Load(json::JSON &jPlayerBoard, json::JSON &jBotBoard)
{
	// Load Player Board
	for (int i = 0; i < jPlayerBoard.size(); i++) {
		playerBoard.push_back(new Card(Resources::getInstance()->GetCard(jPlayerBoard[i].ToString())));
	}

	// Load Bot Board
	for (int i = 0; i < jBotBoard.size(); i++) {
		botBoard.push_back(new Card(Resources::getInstance()->GetCard(jBotBoard[i].ToString())));
	}
}

void Board::GetCardTotals(std::vector<Card*>* board, int* melee, int* ranged, int* siege) {
	for (int i = 0; i < board->size(); i++) {
		if ((*board)[i]->GetClass() == CardClass::Melee) {
			(*melee)++;
		}
		else if ((*board)[i]->GetClass() == CardClass::Ranged) {
			(*ranged)++;
		}
		else {
			(*siege)++;
		}
	}
}
