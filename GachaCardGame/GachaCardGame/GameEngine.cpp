#include "GameEngine.h"
#include "Resources.h"
#include "SqlManager.h"

#include <Windows.h>
#include <fstream>
#include <iostream>

GameEngine::GameEngine() {
	board = new Board(this);
	player = new Player();
	bot = new Bot();
	gameState = GameState::StartScreen;
    playerPass = false;
    playerWins = 0;
    botWins = 0;
    activePlayerDeck = 0;
    activeBotDeck = 0;

    // Initialize resources
    Resources::getInstance();

    // Initialize SqlManager
    SqlManager::getInstance();

    SetupStartDisplay();
    SetupEndGameDisplay();
}

GameEngine::~GameEngine() {
    delete(board);
    delete(player);
    delete(bot);
}

void GameEngine::GameLoop() {
	// First create the window
	sf::RenderWindow window(sf::VideoMode(1920, 1080), "Gacha Cards");
    ::ShowWindow(window.getSystemHandle(), SW_MAXIMIZE);

    // run the program as long as the window is open
    while (window.isOpen())
    {
        // check all the window's events that were triggered since the last iteration of the loop
        sf::Event event;
        while (window.pollEvent(event))
        {
            // "close requested" event: we close the window
            if (event.type == sf::Event::Closed)
                window.close();

            // Mouse click events
            if (event.type == sf::Event::MouseButtonPressed) {
                // transform the mouse position from window coordinates to world coordinates
                sf::Vector2f mouseCoords = window.mapPixelToCoords(sf::Mouse::getPosition(window));
                HandleMouseClick(mouseCoords, window);
            }
        }

        // clear the window with black color
        window.clear(sf::Color::Black);

        // draw everything here...
        if (gameState == GameState::StartScreen) {
            DisplayStartScreen(window);
        } 
        else if (gameState == GameState::BotTurn || gameState == GameState::PlayerTurn) {
            board->Display(window);
            if (gameState == GameState::BotTurn) {
                bool dontPass = false;
                if (!bot->TakeTurn(board)) {
                    // If player also passed, then round over
                    if (playerPass) {
                        // end round
                        EndRound();
                        dontPass = true;
                    }
                }
                // Player turn next
                if (!dontPass) {
                    gameState = GameState::PlayerTurn;
                }
            }
            player->Display(window);
            // bot doesn't display anything
        }
        else { // end game screen
            DisplayEndGameScreen(window);
        }

        // end the current frame
        window.display();
    }
}

void GameEngine::HandleMouseClick(sf::Vector2f coords, sf::RenderWindow &window) {
    if (gameState == GameState::StartScreen) {
        HandleStartClick(coords, window);
    }
    else if (gameState == GameState::PlayerTurn) {
        HandleGameClick(coords, window);

    }
    else { // end game screen
        HandleEndClick(coords, window);
    }
}

void GameEngine::HandleStartClick(sf::Vector2f& coords, sf::RenderWindow& window)
{
    // check if they clicked change deck for player
    // check if they clicked change deck for bot
    // check if they clicked start
    // check if they clicked quit
    if (playerRandomRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(true, 0);
    }
    else if (playerGoodRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(true, 1);
    }
    else if (playerBadRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(true, 2);
    }
    else if (playerOPRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(true, 3);
    }
    else if (botRandomRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(false, 0);
    }
    else if (botGoodRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(false, 1);
    }
    else if (botBadRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(false, 2);
    }
    else if (botOPRect.getGlobalBounds().contains(coords)) {
        SwitchDeck(false, 3);
    }
    else if (playRect.getGlobalBounds().contains(coords)) {
        TransitionToGame();
    }
    else if (loadRect.getGlobalBounds().contains(coords)) {
        LoadGame();
    }
    else if (quitRect.getGlobalBounds().contains(coords)) {
        window.close();
    }
}

void GameEngine::HandleGameClick(sf::Vector2f& coords, sf::RenderWindow& window)
{
    // check if they clicked pass turn
    if (board->ClickedPass(coords)) {
        // if we were in the destroy state, then we played a card
        // if not, then we just passed the turn
        if (!destroyState) {
            playerPass = true;
        }

        // Destroy state over
        SetDestroyState(false);

        gameState = GameState::BotTurn;
        return;
    }

    // check if they clicked a card in their hand
    Card* clickedCard = player->ClickedHand(coords);
    if (clickedCard != NULL && !destroyState) {
        // Player clicked a card in their hand
        player->PlayCard(clickedCard, board);
        // Player did NOT pass
        playerPass = false;
        // If destroy state was set, don't pass yet
        if (!destroyState) {
            // Transition to bot
            gameState = GameState::BotTurn;
        }
    }

    // Check if they clicked save game
    if (board->ClickedSave(coords)) {
        SaveGame();
    }

    if (destroyState) {
        // Did we click an opp card to destroy?
        Card* oppCard = board->ClickedOppBoard(coords);
        if (oppCard != NULL) {
            // Remove card from opp board
            board->RemoveCard(false, oppCard);
            // destroy state is over
            SetDestroyState(false);
            // now bot's turn
            gameState = GameState::BotTurn;
        }
    }
}

void GameEngine::HandleEndClick(sf::Vector2f& coords, sf::RenderWindow& window)
{
    // Check if clicked quit or play again
    sf::FloatRect quitBounds = egQuitRect.getGlobalBounds();
    if (quitBounds.contains(coords)) {
        window.close();
        return;
    }

    sf::FloatRect playAgainBounds = playAgainRect.getGlobalBounds();
    if (playAgainBounds.contains(coords)) {
        // play again
        // Clear all cards
        board->ClearBoard();
        bot->ClearDeckAndHand();
        player->ClearDeckAndHand();

        // Transition back to gamestate
        TransitionToGame();
    }
}

void GameEngine::TransitionToGame() {
    // Set score to 0
    playerWins = 0;
    botWins = 0;

    // Reset participant's data
    player->ResetData(true);
    bot->ResetData(true);

    // Create player deck
    if (activePlayerDeck == 0) {
        player->CreateDeck();
    }
    else {
        player->LoadDeck(GetDeckString(activePlayerDeck));
    }
    // Add 4 cards from top of deck to player hand
    player->DrawCard();
    player->DrawCard();
    player->DrawCard();
    player->DrawCard();

    // Create bot deck
    if (activeBotDeck == 0) {
        bot->CreateDeck();
    }
    else {
        bot->LoadDeck(GetDeckString(activeBotDeck));
    }
    // Add 4 cards from bot deck to bot hand
    bot->DrawCard();
    bot->DrawCard();
    bot->DrawCard();
    bot->DrawCard();

    // set to player turn
    gameState = GameState::PlayerTurn;
}

void GameEngine::EndRound() {
    // Award point to whoever has more points on board
    if (board->GetBotScore() > board->GetPlayerScore()) {
        botWins++;
    }
    else {
        playerWins++;
    }

    // Record Data, then reset
    SqlManager::getInstance()->RecordCardsPlayed(bot->GetCardsPlayed() + player->GetCardsPlayed(), botWins + playerWins);
    player->ResetData(false);
    bot->ResetData(false);

    // Did someone reach 2 wins?
    if (playerWins >= 2 || botWins >= 2) {
        // Game over
        gameState = GameState::EndGame;
        // Record who won
        SqlManager::getInstance()->RecordWin(playerWins >= botWins);
        // Record other data
        SqlManager::getInstance()->RecordRarities(
            player->GetCommonsPlayed() + bot->GetCommonsPlayed(),
            player->GetUncommonsPlayed() + bot->GetUncommonsPlayed(),
            player->GetRaresPlayed() + bot->GetRaresPlayed());
        SqlManager::getInstance()->RecordDestroyed(player->GetDestroyedPlayed(), bot->GetDestroyedPlayed());
    }
    else {
        // clear the board
        board->ClearBoard();
        // Set to player turn
        gameState = GameState::PlayerTurn;
    }
}

int GameEngine::GetBotWins() {
    return botWins;
}

int GameEngine::GetPlayerWins() {
    return playerWins;
}

void GameEngine::SetupEndGameDisplay() {
    playAgainRect.setFillColor(sf::Color(0, 255, 0));
    playAgainRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playAgainRect.setPosition(sf::Vector2f(635.0f, 630.0f));
    
    playAgainText.setPosition(sf::Vector2f(675.0f, 638.0f));
    playAgainText.setCharacterSize(35);
    playAgainText.setFont((*Resources::getInstance()->GetBaseFont()));
    playAgainText.setString("Play Again");
    playAgainText.setFillColor(sf::Color());

    egQuitRect.setFillColor(sf::Color(255, 0, 0));
    egQuitRect.setSize(sf::Vector2f(250.0f, 60.0f));
    egQuitRect.setPosition(sf::Vector2f(1035.0f, 630.0f));

    egQuitText.setPosition(sf::Vector2f(1130.0f, 637.0f));
    egQuitText.setCharacterSize(35);
    egQuitText.setFont((*Resources::getInstance()->GetBaseFont()));
    egQuitText.setString("Quit");
    egQuitText.setFillColor(sf::Color());

    winnerText.setPosition(sf::Vector2f(835.0f, 430.0f));
    winnerText.setCharacterSize(50);
    winnerText.setFont((*Resources::getInstance()->GetBaseFont()));
}

void GameEngine::DisplayEndGameScreen(sf::RenderWindow &window) {
    // Set winner text and color
    if (botWins > playerWins) {
        winnerText.setString("Opponent Wins!");
        winnerText.setFillColor(sf::Color(255, 0, 0));
    }
    else {
        winnerText.setString("Player Wins!");
        winnerText.setFillColor(sf::Color(0, 255, 0));
    }
    window.draw(winnerText);

    // Buttons
    window.draw(playAgainRect);
    window.draw(playAgainText);
    window.draw(egQuitRect);
    window.draw(egQuitText);
}

void GameEngine::SetupStartDisplay() {

    // Title
    gameTitle.setString("Gacha Cards");
    gameTitle.setPosition(700.0f, 100.0f);
    gameTitle.setFont((*Resources::getInstance()->GetBaseFont()));
    gameTitle.setFillColor(sf::Color(255, 0, 0));
    gameTitle.setCharacterSize(100);

#pragma region Play, Load, Quit Buttons
    playRect.setFillColor(sf::Color(0, 255, 0));
    playRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playRect.setPosition(sf::Vector2f(385.0f, 930.0f));

    playText.setPosition(sf::Vector2f(475.0f, 938.0f));
    playText.setCharacterSize(35);
    playText.setFont((*Resources::getInstance()->GetBaseFont()));
    playText.setString("Play");
    playText.setFillColor(sf::Color());    
    
    loadRect.setFillColor(sf::Color(0, 0, 255));
    loadRect.setSize(sf::Vector2f(250.0f, 60.0f));
    loadRect.setPosition(sf::Vector2f(835.0f, 930.0f));

    loadText.setPosition(sf::Vector2f(915.0f, 938.0f));
    loadText.setCharacterSize(35);
    loadText.setFont((*Resources::getInstance()->GetBaseFont()));
    loadText.setString("Load");
    loadText.setFillColor(sf::Color());

    quitRect.setFillColor(sf::Color(255, 0, 0));
    quitRect.setSize(sf::Vector2f(250.0f, 60.0f));
    quitRect.setPosition(sf::Vector2f(1285.0f, 930.0f));

    quitText.setPosition(sf::Vector2f(1385.0f, 937.0f));
    quitText.setCharacterSize(35);
    quitText.setFont((*Resources::getInstance()->GetBaseFont()));
    quitText.setString("Quit");
    quitText.setFillColor(sf::Color());
#pragma endregion Play, Load, Quit Buttons

#pragma region Deck Titles

    randomDeck.setCharacterSize(35);
    randomDeck.setFont((*Resources::getInstance()->GetBaseFont()));
    randomDeck.setString("Random");
    randomDeck.setFillColor(sf::Color());

    goodDeck.setCharacterSize(35);
    goodDeck.setFont((*Resources::getInstance()->GetBaseFont()));
    goodDeck.setString("The Good");
    goodDeck.setFillColor(sf::Color());

    badDeck.setCharacterSize(35);
    badDeck.setFont((*Resources::getInstance()->GetBaseFont()));
    badDeck.setString("The Bad");
    badDeck.setFillColor(sf::Color());

    opDeck.setCharacterSize(35);
    opDeck.setFont((*Resources::getInstance()->GetBaseFont()));
    opDeck.setString("The OP");
    opDeck.setFillColor(sf::Color());

#pragma endregion Deck Titles

#pragma region Player Decks

    playerText.setCharacterSize(35);
    playerText.setPosition(sf::Vector2f(915.0f, 350.0f));
    playerText.setFont((*Resources::getInstance()->GetBaseFont()));
    playerText.setString("Player");
    playerText.setFillColor(sf::Color(255, 255, 255));

    playerRandomRect.setFillColor(sf::Color(125, 125, 125));
    playerRandomRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playerRandomRect.setPosition(sf::Vector2f(385.0f, 450.0f));

    playerGoodRect.setFillColor(sf::Color(125, 125, 125));
    playerGoodRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playerGoodRect.setPosition(sf::Vector2f(685.0f, 450.0f));

    playerBadRect.setFillColor(sf::Color(125, 125, 125));
    playerBadRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playerBadRect.setPosition(sf::Vector2f(985.0f, 450.0f));

    playerOPRect.setFillColor(sf::Color(125, 125, 125));
    playerOPRect.setSize(sf::Vector2f(250.0f, 60.0f));
    playerOPRect.setPosition(sf::Vector2f(1285.0f, 450.0f));

#pragma endregion Player Decks

#pragma region Bot Decks

    botText.setCharacterSize(35);
    botText.setPosition(sf::Vector2f(890.0f, 600.0f));
    botText.setFont((*Resources::getInstance()->GetBaseFont()));
    botText.setString("Opponent");
    botText.setFillColor(sf::Color(255, 255, 255));

    botRandomRect.setFillColor(sf::Color(125, 125, 125));
    botRandomRect.setSize(sf::Vector2f(250.0f, 60.0f));
    botRandomRect.setPosition(sf::Vector2f(385.0f, 700.0f));

    botGoodRect.setFillColor(sf::Color(125, 125, 125));
    botGoodRect.setSize(sf::Vector2f(250.0f, 60.0f));
    botGoodRect.setPosition(sf::Vector2f(685.0f, 700.0f));

    botBadRect.setFillColor(sf::Color(125, 125, 125));
    botBadRect.setSize(sf::Vector2f(250.0f, 60.0f));
    botBadRect.setPosition(sf::Vector2f(985.0f, 700.0f));

    botOPRect.setFillColor(sf::Color(125, 125, 125));
    botOPRect.setSize(sf::Vector2f(250.0f, 60.0f));
    botOPRect.setPosition(sf::Vector2f(1285.0f, 700.0f));
#pragma endregion Bot Decks

    UpdateDeckColours();
}

void GameEngine::DisplayStartScreen(sf::RenderWindow &window) {

    // Title
    window.draw(gameTitle);

    // Play, Load, Quit Buttons
    window.draw(playRect);
    window.draw(playText);
    window.draw(loadRect);
    window.draw(loadText);
    window.draw(quitRect);
    window.draw(quitText);

    // Player Decks
    window.draw(playerText);
    window.draw(playerRandomRect);
    window.draw(playerGoodRect);
    window.draw(playerBadRect);
    window.draw(playerOPRect);

    // Draw Player Deck texts
    randomDeck.setPosition(sf::Vector2f(playerRandomRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(randomDeck);
    goodDeck.setPosition(sf::Vector2f(playerGoodRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(goodDeck);
    badDeck.setPosition(sf::Vector2f(playerBadRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(badDeck);
    opDeck.setPosition(sf::Vector2f(playerOPRect.getPosition() + sf::Vector2f(55.0f, 10.0f)));
    window.draw(opDeck);

    // Bot decks
    window.draw(botText);
    window.draw(botRandomRect);
    window.draw(botGoodRect);
    window.draw(botBadRect);
    window.draw(botOPRect);

    // Draw Bot Deck texts
    randomDeck.setPosition(sf::Vector2f(botRandomRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(randomDeck);
    goodDeck.setPosition(sf::Vector2f(botGoodRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(goodDeck);
    badDeck.setPosition(sf::Vector2f(botBadRect.getPosition() + sf::Vector2f(50.0f, 10.0f)));
    window.draw(badDeck);
    opDeck.setPosition(sf::Vector2f(botOPRect.getPosition() + sf::Vector2f(55.0f, 10.0f)));
    window.draw(opDeck);
}

void GameEngine::SwitchDeck(bool player, int deck) {
    if (player) {
        activePlayerDeck = deck;
    }
    else {
        activeBotDeck = deck;
    }

    UpdateDeckColours();
}

void GameEngine::UpdateDeckColours() {
    // Player decks
    playerRandomRect.setFillColor(activePlayerDeck == 0 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    playerGoodRect.setFillColor(activePlayerDeck == 1 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    playerBadRect.setFillColor(activePlayerDeck == 2 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    playerOPRect.setFillColor(activePlayerDeck == 3 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));

    // Bot decks
    botRandomRect.setFillColor(activeBotDeck == 0 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    botGoodRect.setFillColor(activeBotDeck == 1 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    botBadRect.setFillColor(activeBotDeck == 2 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
    botOPRect.setFillColor(activeBotDeck == 3 ? sf::Color(0, 255, 255) : sf::Color(125, 125, 125));
}

std::string GameEngine::GetDeckString(int deck) {
    if (deck == 1) {
        // The Good
        return "deckGood.json";
    }
    else if (deck == 2) {
        // The Bad
        return "deckBad.json";
    }
    else {
        // The OP
        return "deckOP.json";
    }
}

void GameEngine::SetDestroyState(bool state) {
    destroyState = state;
}

bool GameEngine::GetDestroyState() {
    return destroyState;
}

void GameEngine::SaveGame() {
    // Create JSON
    json::JSON document;

    // Save board
    board->Save(document);

    // Save player
    player->Save(document);

    // Save bot
    bot->Save(document);

    // Save score
    document["PlayerWins"] = playerWins;
    document["BotWins"] = botWins;
    
    // Destroy state
    document["DestroyState"] = destroyState;

    // Create ofstream
    std::ofstream ostrm(SaveFile);
    ostrm << document.dump() << std::endl;
}

void GameEngine::LoadGame()
{
    // Read in JSON
    // Load the deck from the given path
    json::JSON savedGame;
    try {
        std::ifstream inputStreamSettings(SaveFile);
        std::string cardListString((std::istreambuf_iterator<char>(inputStreamSettings)),
            std::istreambuf_iterator<char>());

        savedGame = json::JSON::Load(cardListString);
    }
    catch(int e) {
        // Fails for any reason, just return
        return;
    }

    // Check json
    if (!savedGame.hasKey("PlayerBoard") || 
        !savedGame.hasKey("BotBoard") ||
        !savedGame.hasKey("Player") ||
        !savedGame.hasKey("Bot") ||
        !savedGame.hasKey("PlayerWins") ||
        !savedGame.hasKey("BotWins") ||
        !savedGame.hasKey("DestroyState")) {
        std::cout << "Invalid save state" << std::endl;
        return;
    }

    // Load in the board
    board->Load(savedGame["PlayerBoard"], savedGame["BotBoard"]);

    // Load in the player
    if (!player->Load(savedGame["Player"])) {
        return;
    }

    // Load in the bot
    if (!bot->Load(savedGame["Bot"])) {
        return;
    }
    
    // Set score
    playerWins = savedGame["PlayerWins"].ToInt();
    botWins = savedGame["BotWins"].ToInt();

    // Set destroystate
    destroyState = savedGame["DestroyState"].ToBool();

    // Player always starts
    gameState = GameState::PlayerTurn;
}
