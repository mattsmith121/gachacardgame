#ifndef _RESOURCES_H_
#define _RESOURCES_H_

#include "Card.h"
#include "CardData.h"
#include <string>
#include <SFML/Graphics.hpp>
#include <vector>

class Resources {

	// Struct for storing list of cards
	struct CardList {
	public:
		std::vector<CardData*> commons;
		std::vector<CardData*> uncommons;
		std::vector<CardData*> rares;
	};
	CardList cardList;
	void CheckInitialized();

	static Resources* instance;
	const std::string cardBorderFile = "Card.png";
	const std::string meleeIconFile = "Melee.png";
	const std::string siegeIconFile = "Siege.png";
	const std::string rangeIconFile = "Ranged.png";
	const std::string valueIconFile = "ValueIcon.png";

	// Base Font
	sf::Font baseFont;

	// Card textures
	sf::Texture cardBorderArt;
	sf::Texture meleeIcon;
	sf::Texture rangeIcon;
	sf::Texture siegeIcon;
	sf::Texture valueIcon;

	Resources();

public:
	static Resources* getInstance();

	void LoadCardList();

	sf::Font* GetBaseFont();

	sf::Texture* GetCardBorderArt();
	sf::Texture* GetMeleeIcon();
	sf::Texture* GetRangeIcon();
	sf::Texture* GetSiegeIcon();
	sf::Texture* GetValueIcon();
	sf::Texture* GetClassIcon(CardClass cardClass);
	std::vector<CardData*>* GetCommons();
	std::vector<CardData*>* GetUncommons();
	std::vector<CardData*>* GetRares();
	CardData* GetCard(std::string cardName);
	CardData* GetCard(int id);
};

#endif // !_RESOURCES_H_

