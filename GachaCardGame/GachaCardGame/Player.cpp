#include "Player.h"
#include <iostream>

Player::Player() {
	player = true;
}

Player::~Player() {

}

void Player::Display(sf::RenderWindow &window) {
	// Display hand
	int spacer = 230.0f;
	for (int i = 0; i < hand.size(); i++) {
		Card* temp = hand[i];
		temp->SetPosition(sf::Vector2f(50.0f, handPosTop + i * spacer));
		temp->Display(window);
	}
}

void Player::Save(json::JSON& document)
{
	// Create player object
	json::JSON player = json::JSON::Object();

	// Save hand and deck
	Participant::Save(player);

	document["Player"] = player;
}

bool Player::Load(json::JSON &jPlayer)
{
	// Load in deck and hand
	return Participant::Load(jPlayer);
}

Card* Player::ClickedHand(sf::Vector2f coords) {
	for (int i = 0; i < hand.size(); i++) {
		// get bounds of card
		sf::FloatRect bounds = hand[i]->GetSpriteBounds();

		if (bounds.contains(coords)) {
			// clicked this card
			return hand[i];
		}
	}

	return NULL;
}