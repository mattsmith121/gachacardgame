#include "Bot.h"
#include <stdlib.h>
#include <chrono>

Bot::Bot() {
	player = false;
}

Bot::~Bot() {

}

bool Bot::TakeTurn(Board* board) {
	// Logic for bot taking turn
	// GameEngine transitions turn afte this function returns

	// If computer is out of cards it will pass
	if (hand.size() <= 0) {
		return false;
	}
	// If computer has played 6 cards this round it will pass
	else if (cardsPlayed >= 6) {
		return false;
	} 
	// If computer is winning by more than 5 it will pass
	else if (board->GetBotScore() - board->GetPlayerScore() > 5) {
		return false;
	}
	// If computer is losing by more than 7 it will pass
	else if (board->GetBotScore() - board->GetPlayerScore() < -7) {
		return false;
	}

	// Otherwise randomly play a card
	Card* cardSelected;
	// Get identifier
	srand(std::chrono::system_clock::now().time_since_epoch().count());
	int index = (rand() % hand.size()); // Random card in hand
	PlayCard(hand[index], board);
	return true;
}

void Bot::Save(json::JSON &document)
{
	// Create bot object
	json::JSON bot = json::JSON::Object();

	// Save hand and deck
	Participant::Save(bot);

	document["Bot"] = bot;
}

bool Bot::Load(json::JSON& jBot)
{
	// Load in deck and hand
	return Participant::Load(jBot);
}
