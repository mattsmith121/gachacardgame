#ifndef _BOT_H_
#define _BOT_H_

#include "Participant.h"

class Bot : public Participant {

public:
	Bot();
	~Bot();
	bool TakeTurn(Board* board);
	void Save(json::JSON &document) override;
	bool Load(json::JSON &jBot);
};

#endif // !_BOT_H_

